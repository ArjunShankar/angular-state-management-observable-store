export interface Customer {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  company: Company;
  phone: string;
  website: string;
}

export interface Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: LatLong;
}

export interface LatLong {
  lat: number;
  long: number;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}
