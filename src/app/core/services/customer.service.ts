import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CustomerStore } from '../store/customer-store';
import { HttpClient } from '@angular/common/http';
import {Customer} from '../../shared/interfaces';


export enum CustomersStoreActions {
  GetCustomers = 'get_customers'
}

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends ObservableStore<CustomerStore> {

  remoteURL = 'http://jsonplaceholder.typicode.com/users/';

  constructor(private http: HttpClient) {
    super( { trackStateHistory: true }); // todo - how to set initial state "initialStoreState" if needed
  }

  get() {
    const currentState = this.getState();
    if (currentState && currentState.customers) {
      return of(currentState.customers);  // of turns it into an observable
    }
    else {
      return this.getCustomersFromServer();
    }
  }
/*
  add(customer: Customer) {
    // Get state from store
    let state = this.getState();
    state.customers.push(customer);
    // Set state in store
    this.setState({ customers: state.customers },  'add_customer');
  }

  remove() {
    // Get state from store
    let state = this.getState();
    state.customers.splice(state.customers.length - 1, 1);
    // Set state in store
    this.setState({ customers: state.customers }, 'remove_customer');
  }
*/
  private getCustomersFromServer() {
    return this.http.get<Customer[]>(this.remoteURL)
      .pipe(
        map(customers => {
          this.setState({ customers }, CustomersStoreActions.GetCustomers);   // set state in store, return at same time
          return customers;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }


}
