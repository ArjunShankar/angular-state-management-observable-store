import {Component, OnDestroy, OnInit} from '@angular/core';
import {CustomerService} from '../../../core/services/customer.service';
import {Customer} from '../../../shared/interfaces';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-customer-landing-page',
  templateUrl: './customer-landing-page.component.html',
  styleUrls: ['./customer-landing-page.component.css']
})
export class CustomerLandingPageComponent implements OnInit, OnDestroy {

  dataProvider: Array<Customer>;
  storeSub: Subscription;
  displayedColumns: Array<string>;

  constructor(
    private customerService: CustomerService
  ) {
    this.displayedColumns = ['id', 'name', 'username', 'email', 'actions'];
}

  ngOnInit() {
    this.storeSub = this.customerService.get().subscribe((data) => {
      this.dataProvider = data;
    });
  }

  actionClicked(event, userId) {
    console.log(userId);
  }

  ngOnDestroy() {
    if (this.storeSub) {
      this.storeSub.unsubscribe();
    }
  }

}
