import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerLandingPageComponent } from './components/customer-landing-page/customer-landing-page.component';
import { MatTableModule} from '@angular/material/table';
import {MatButtonModule, MatIconModule} from '@angular/material';

@NgModule({
  declarations: [CustomerLandingPageComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [CustomerLandingPageComponent]
})
export class CustomerModule { }
