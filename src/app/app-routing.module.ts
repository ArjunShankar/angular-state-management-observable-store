import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerLandingPageComponent } from './customer/components/customer-landing-page/customer-landing-page.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: CustomerLandingPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
